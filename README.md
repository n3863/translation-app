<div id="top"></div>

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://github.com/github_username/repo_name">
    <img src="./public/img/ui/Logo-Hello.png" alt="Logo" width="80" height="80">
  </a>

<h3 align="center">Lost in Translation</h3>

  <p align="center">
    Build an translation app using react.
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li><a href="#getting-started">Getting Started</li>
    <li><a href="#functionality">Functionality</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

 <a href="https://github.com/github_username/repo_name">
    <img src="./public/img/translationApp.png" alt="app">
  </a>

This is a my first React Assignment Project for the Noroff Java Bootcamp.
Where we were tasked with creating a minor transaltion app that consisted of a login screen with login features, profile page and translation page where one can enter text for it to be translated into signlanguage. .

<p align="right">(<a href="#top">back to top</a>)</p>



### Built With

* [JS](https://www.javascript.com/)
* [HTML5](https://developer.mozilla.org/en-US/docs/Glossary/HTML5)
* [CSS3](https://developer.mozilla.org/en-US/docs/Web/CSS)
* [React](https://reactjs.org/)
* [Heroku](https://fast-shore-15130.herokuapp.com/profile)

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- GETTING STARTED -->
## Getting Started

This project uses node and npm. Go check them out if you don't have them locally installed.
Project is also hoste on [Heroku](https://fast-shore-15130.herokuapp.com/profile)

```sh
$ npm init
# Install the dependencies for the project.
$ npm start
# Run a local React server
```


<!-- USAGE EXAMPLES -->
## Functionality

<p>Login:</p>
<ul>
<li>Checks if user already exists</li>
<li>Logs them in if they do exist</li>
<li>Creates a new user if they do not exist</li>
</ul>
<p>Translate:</p>
<ul>
<li>Enter a word or sentance to have it translated</li>
<li>Translations get saved to users account</li>
</ul>
<p>Profile</p>
<ul>
<li>Can see an overview of translations history</li>
<li>Can remove all translation history</li>
<li>Can logout</li>
</ul>
<p align="right">(<a href="#top">back to top</a>)</p>



<!-- ROADMAP -->






<!-- CONTRIBUTING -->






<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE.txt` for more information.

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- CONTACT -->
## Contact

Axl Branco Duarte - axl.nl@hotmail.com
Project Link: [https://github.com/github_username/AxlBrancoDuarte](https://github.com/github_username/repo_name)

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- ACKNOWLEDGMENTS -->





<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
