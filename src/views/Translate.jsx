import { useState, useEffect } from "react"
import { updateTranslations } from "../api/translation"
import TranslateForm from "../components/Translate/TranslateForm"
import { STORAGE_KEY_USER } from "../const/storageKeys"
import { useUser } from "../context/UserContext"
import withAuth from "../hoc/withAuth"
import { storageSave } from "../utils/storage"
import "./views.css"

const Translate = () => {

    const [requestedTransltation, setrequestedTransltation] = useState([])
    const { user } = useUser()

    useEffect(() => {
        if (requestedTransltation && requestedTransltation.length > 0) {
            user.translations = [...user.translations, requestedTransltation.join("")]
            console.log(user)
            storageSave(STORAGE_KEY_USER, user)
        }
    }, [requestedTransltation]);
 

    const handleTranslateClicked =async({ translationRequest }) => {
        setrequestedTransltation(translationRequest.split(""))
        
        const [error,result] = await updateTranslations(user, translationRequest)
       
        console.log('Error', error)
        console.log('Result', result)
    }

  

    let hands = requestedTransltation.map(letter => <li key={letter}><img src={`img/signs/${letter.toLowerCase()}.png`} /></li>)

    return (
        <div className='container'>
            <h1>Translate</h1>
            <section>
                <TranslateForm onTranslateRequest={handleTranslateClicked} />
                <ul className="flex-row-hands">{hands}</ul>
            </section>
        </div>
    )
}

export default withAuth(Translate)