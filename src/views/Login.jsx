import LoginForm from '../components/Login/LoginForm'
import "./views.css"

const Login = () => {
    return(
        <div className='container'>
        <LoginForm />
        </div>
        
    )
}

export default Login