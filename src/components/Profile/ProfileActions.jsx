import { clearTranslations } from "../../api/translation"
import { STORAGE_KEY_USER } from "../../const/storageKeys"
import { useUser } from "../../context/UserContext"
import { storageDelete, storageSave} from "../../utils/storage"


const ProfileActions = () => {

    const {user, setUser } = useUser()

    const handelLogoutClick = () => {
        if (window.confirm("Are you sure you want to logout?")){
            storageDelete(STORAGE_KEY_USER)
            setUser(null)
        }
    }

    const handleClearTranslations = () => {
        clearTranslations(user)
        setUser(user => ({...user, translations:[]}))
        storageSave(STORAGE_KEY_USER, {...user,translations:[] })
    }

    return(
        <ul>
            <li><button className="button-style" onClick={handleClearTranslations} >Clear Translations</button></li>
            <li><button className="button-style" onClick={ handelLogoutClick }>Logout</button></li>
        </ul>
    )
}
export default ProfileActions