import ProfileTranslationHistoryItem from "./ProfileTranslationHistoryItem"
import "./profile.css"

const ProfileTranslateHistory = ({translations}) => {

    const translationList = translations.map( 
        (translation, index) => <ProfileTranslationHistoryItem key={ index +":"+ translation } translation ={ translation }/>)

    return(
        <section>
            <h4>Your transaltion history.</h4>
            <ul className="no-style">
                {translationList}  
            </ul>
        </section>
    )
}
export default ProfileTranslateHistory