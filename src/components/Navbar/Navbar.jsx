import "./navbar.css"
import { NavLink } from "react-router-dom"
import { useUser } from "../../context/UserContext"



const Navbar = () => {

    const { user } = useUser()

    return (
        <nav >
          
            <ul className="nav-links ">
                <li className="app-name">
                   <p>Lost in Translation</p>
                </li>
            </ul>
            { user !== null &&
                <ul className="nav-links">
                    <li>
                        <NavLink to="/translator" >Translator</NavLink>
                    </li>
                    <li>
                        <NavLink to="/profile">Profile</NavLink>
                    </li>
                </ul>
            }
           
        </nav>
    )
}
export default Navbar