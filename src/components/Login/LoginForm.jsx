import { useForm } from "react-hook-form"
import { loginUser } from "../../api/user"
import { useEffect, useState } from "react"
import { storageSave } from "../../utils/storage"
import { useNavigate } from "react-router-dom"
import { useUser } from "../../context/UserContext"
import { STORAGE_KEY_USER } from "../../const/storageKeys"
import "./login.css"

const usernameConfig = {
    required: true,
    minLength: 3,
};

const LoginForm = () => {
    //Hooks
    const { register, handleSubmit, formState: { errors }, } = useForm();
    const { user, setUser } = useUser();
    const navigate = useNavigate()

    //local State
    const [loading, setLoading] = useState(false);
    const [apiError, setApiError] = useState(null);

    //Side Effects
    useEffect(() => {
        if (user !== null) {
            navigate("profile")
        }
    }, [user, navigate]) //Empty Dependencies - Only run once

    //Event Handlers
    const onSubmit = async ({ username }) => {
        setLoading(true);
        const [error, userResponse] = await loginUser(username);
        if (error !== null) {
            setApiError(error);
        }
        if (userResponse !== null) {
            storageSave(STORAGE_KEY_USER, userResponse)
            setUser(userResponse)
        }
        setLoading(false);

    };

    //Render Function
    const errorMessage = () => {
        if (!errors.username) {
            return null;
        }
        if (errors.username.type === "required") {
            return <span>Username is required!</span>;
        }
        if (errors.username.type === "minLength") {
            return <span>Username too short, cannot be less then 3 characters!</span>;
        }
    };

    return (
        <>
        <div className="flex-row">
            <img src="img/ui/Logo-Hello.png" alt="" />
            <div className="center">
                <h2>Lost In Translation</h2>
                <h4>Get Started</h4>
            </div>
        </div>
            <form onSubmit={handleSubmit(onSubmit)}>
                <fieldset>
                    <label htmlFor="username">Username:</label>

                    <input
                        type="text"
                        placeholder="What's your name?"
                        {...register("username", usernameConfig)}
                    />
                    {errorMessage()}
                    <button className="button-style-arrow" type="submit" disabled={loading}>
                        Enter
                    </button>
                </fieldset>

                {loading && <p>Logging in...</p>}
                {apiError && <p>{apiError}</p>}
            </form>
        </>
    );
};

export default LoginForm;
